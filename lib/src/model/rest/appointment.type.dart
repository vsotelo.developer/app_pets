// To parse this JSON data, do
//
//     final appointmentTypeRs = appointmentTypeRsFromMap(jsonString);

import 'dart:convert';

class AppointmentTypeRs {
    AppointmentTypeRs({
        required this.message,
        required this.result,
    });

    final String message;
    final List<AppointmentType> result;

    factory AppointmentTypeRs.fromJson(String str) => AppointmentTypeRs.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory AppointmentTypeRs.fromMap(Map<String, dynamic> json) => AppointmentTypeRs(
        message: json["message"],
        result: List<AppointmentType>.from(json["result"].map((x) => AppointmentType.fromMap(x))),
    );

    Map<String, dynamic> toMap() => {
        "message": message,
        "result": List<dynamic>.from(result.map((x) => x.toMap())),
    };
}

class AppointmentType {
    AppointmentType({
        required this.id,
        required this.name,
        required this.scheduleList,
    });

    final String id;
    final String name;
    final List<ScheduleList> scheduleList;

    factory AppointmentType.fromJson(String str) => AppointmentType.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory AppointmentType.fromMap(Map<String, dynamic> json) => AppointmentType(
        id: json["id"],
        name: json["name"],
        scheduleList: List<ScheduleList>.from(json["scheduleList"].map((x) => ScheduleList.fromMap(x))),
    );

    Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
        "scheduleList": List<dynamic>.from(scheduleList.map((x) => x.toMap())),
    };
}

class ScheduleList {
    ScheduleList({
        required this.day,
        required this.hours,
        required this.id,
    });

    final int day;
    final List<int> hours;
    final String id;

    factory ScheduleList.fromJson(String str) => ScheduleList.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory ScheduleList.fromMap(Map<String, dynamic> json) => ScheduleList(
        day: json["day"],
        hours: List<int>.from(json["hours"].map((x) => x)),
        id: json["id"],
    );

    Map<String, dynamic> toMap() => {
        "day": day,
        "hours": List<dynamic>.from(hours.map((x) => x)),
        "id": id,
    };
}
