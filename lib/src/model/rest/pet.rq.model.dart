// To parse this JSON data, do
//
//     final petRq = petRqFromMap(jsonString);

import 'dart:convert';

class PetRq {
  PetRq({
    required this.age,
    required this.breed,
    required this.gender,
    required this.name,
    required this.specie,
    required this.weight,
  });

  final int age;
  final String breed;
  final String gender;
  final String name;
  final String specie;
  final int weight;

  factory PetRq.fromJson(String str) => PetRq.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory PetRq.fromMap(Map<String, dynamic> json) => PetRq(
        age: json["age"],
        breed: json["breed"],
        gender: json["gender"],
        name: json["name"],
        specie: json["specie"],
        weight: json["weight"],
      );

  Map<String, dynamic> toMap() => {
        "age": age,
        "breed": breed,
        "gender": gender,
        "name": name,
        "specie": specie,
        "weight": weight,
      };
}
