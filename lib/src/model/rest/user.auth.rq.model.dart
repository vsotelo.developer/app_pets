// To parse this JSON data, do
//
//     final userAuthRq = userAuthRqFromMap(jsonString);

import 'dart:convert';

class UserAuthRq {
    UserAuthRq({
        required this.password,
        required this.tokenFcm,
        required this.username,
    });

    final String password;
    final String tokenFcm;
    final String username;

    factory UserAuthRq.fromJson(String str) => UserAuthRq.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory UserAuthRq.fromMap(Map<String, dynamic> json) => UserAuthRq(
        password: json["password"],
        tokenFcm: json["tokenFCM"],
        username: json["username"],
    );

    Map<String, dynamic> toMap() => {
        "password": password,
        "tokenFCM": tokenFcm,
        "username": username,
    };
}
