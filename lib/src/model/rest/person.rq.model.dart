import 'dart:convert';

class PersonRq {
    PersonRq({
        required this.documentNumber,
        required this.emailAddress,
        required this.lastName,
        required this.name,
        required this.phoneNumber,
    });

    final String documentNumber;
    final String emailAddress;
    final String lastName;
    final String name;
    final String phoneNumber;

    factory PersonRq.fromJson(String str) => PersonRq.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory PersonRq.fromMap(Map<String, dynamic> json) => PersonRq(
        documentNumber: json["documentNumber"],
        emailAddress: json["emailAddress"],
        lastName: json["lastName"],
        name: json["name"],
        phoneNumber: json["phoneNumber"],
    );

    Map<String, dynamic> toMap() => {
        "documentNumber": documentNumber,
        "emailAddress": emailAddress,
        "lastName": lastName,
        "name": name,
        "phoneNumber": phoneNumber,
    };
}
