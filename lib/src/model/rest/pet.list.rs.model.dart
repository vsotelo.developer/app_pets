// To parse this JSON data, do
//
//     final petListRs = petListRsFromMap(jsonString);

import 'dart:convert';

class PetListRs {
    PetListRs({
        required this.message,
        required this.result,
    });

    final String message;
    final List<Pet> result;

    factory PetListRs.fromJson(String str) => PetListRs.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory PetListRs.fromMap(Map<String, dynamic> json) => PetListRs(
        message: json["message"],
        result: List<Pet>.from(json["result"].map((x) => Pet.fromMap(x))),
    );

    Map<String, dynamic> toMap() => {
        "message": message,
        "result": List<dynamic>.from(result.map((x) => x.toMap())),
    };
}

class Pet {
    Pet({
        required this.age,
        required this.breed,
        required this.gender,
        required this.id,
        required this.name,
        required this.specie,
        required this.weight,
    });

    final int age;
    final String breed;
    final String gender;
    final String id;
    final String name;
    final String specie;
    final int weight;

    factory Pet.fromJson(String str) => Pet.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory Pet.fromMap(Map<String, dynamic> json) => Pet(
        age: json["age"],
        breed: json["breed"],
        gender: json["gender"],
        id: json["id"],
        name: json["name"],
        specie: json["specie"],
        weight: json["weight"],
    );

    Map<String, dynamic> toMap() => {
        "age": age,
        "breed": breed,
        "gender": gender,
        "id": id,
        "name": name,
        "specie": specie,
        "weight": weight,
    };
}
