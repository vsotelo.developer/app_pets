// To parse this JSON data, do
//
//     final petRs = petRsFromMap(jsonString);

import 'dart:convert';

class PetRs {
    PetRs({
        required this.message,
        required this.result,
    });

    final String message;
    final Result result;

    factory PetRs.fromJson(String str) => PetRs.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory PetRs.fromMap(Map<String, dynamic> json) => PetRs(
        message: json["message"],
        result: Result.fromMap(json["result"]),
    );

    Map<String, dynamic> toMap() => {
        "message": message,
        "result": result.toMap(),
    };
}

class Result {
    Result({
        required this.age,
        required this.breed,
        required this.gender,
        required this.id,
        required this.name,
        required this.specie,
        required this.weight,
    });

    final int age;
    final String breed;
    final String gender;
    final String id;
    final String name;
    final String specie;
    final int weight;

    factory Result.fromJson(String str) => Result.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory Result.fromMap(Map<String, dynamic> json) => Result(
        age: json["age"],
        breed: json["breed"],
        gender: json["gender"],
        id: json["id"],
        name: json["name"],
        specie: json["specie"],
        weight: json["weight"],
    );

    Map<String, dynamic> toMap() => {
        "age": age,
        "breed": breed,
        "gender": gender,
        "id": id,
        "name": name,
        "specie": specie,
        "weight": weight,
    };
}
