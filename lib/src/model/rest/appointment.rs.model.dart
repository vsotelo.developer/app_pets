// To parse this JSON data, do
//
//     final appointmentRs = appointmentRsFromMap(jsonString);

import 'dart:convert';

import 'package:app_pets/src/model/rest/appointment.type.dart';

class AppointmentSaveResponse {
    AppointmentSaveResponse({
        required this.message,
        required this.result,
    });

    final String message;
    final AppointmentRs result;

    factory AppointmentSaveResponse.fromJson(String str) => AppointmentSaveResponse.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory AppointmentSaveResponse.fromMap(Map<String, dynamic> json) => AppointmentSaveResponse(
        message: json["message"],
        result: AppointmentRs.fromMap(json["result"]),
    );

    Map<String, dynamic> toMap() => {
        "message": message,
        "result": result.toMap(),
    };
}

class AppointmentRs {
    AppointmentRs({
        required this.appointmentType,
        required this.details,
        required this.endDate,
        required this.id,
        required this.name,
        required this.startDate,
        required this.status,
    });

    final AppointmentType appointmentType;
    final String details;
    final DateTime endDate;
    final String id;
    final String name;
    final DateTime startDate;
    final bool status;

    factory AppointmentRs.fromJson(String str) => AppointmentRs.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory AppointmentRs.fromMap(Map<String, dynamic> json) => AppointmentRs(
        appointmentType: AppointmentType.fromMap(json["appointmentType"]),
        details: json["details"],
        endDate: DateTime.parse(json["endDate"]),
        id: json["id"],
        name: json["name"],
        startDate: DateTime.parse(json["startDate"]),
        status: json["status"],
    );

    Map<String, dynamic> toMap() => {
        "appointmentType": appointmentType.toMap(),
        "details": details,
        "endDate": endDate.toIso8601String(),
        "id": id,
        "name": name,
        "startDate": startDate.toIso8601String(),
        "status": status,
    };
}

