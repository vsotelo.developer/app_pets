// To parse this JSON data, do
//
//     final personRs = personRsFromMap(jsonString);

import 'dart:convert';

class PersonRs {
    PersonRs({
        required this.message,
        required this.result,
    });

    final String message;
    final Result result;

    factory PersonRs.fromJson(String str) => PersonRs.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory PersonRs.fromMap(Map<String, dynamic> json) => PersonRs(
        message: json["message"],
        result: Result.fromMap(json["result"]),
    );

    Map<String, dynamic> toMap() => {
        "message": message,
        "result": result.toMap(),
    };
}

class Result {
    Result({
        required this.documentNumber,
        required this.emailAddress,
        required this.id,
        required this.lastName,
        required this.name,
        required this.phoneNumber,
    });

    final String documentNumber;
    final String emailAddress;
    final String id;
    final String lastName;
    final String name;
    final String phoneNumber;

    factory Result.fromJson(String str) => Result.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory Result.fromMap(Map<String, dynamic> json) => Result(
        documentNumber: json["documentNumber"],
        emailAddress: json["emailAddress"],
        id: json["id"],
        lastName: json["lastName"],
        name: json["name"],
        phoneNumber: json["phoneNumber"],
    );

    Map<String, dynamic> toMap() => {
        "documentNumber": documentNumber,
        "emailAddress": emailAddress,
        "id": id,
        "lastName": lastName,
        "name": name,
        "phoneNumber": phoneNumber,
    };
}
