// To parse this JSON data, do
//
//     final appointmentRq = appointmentRqFromMap(jsonString);

import 'dart:convert';

import 'package:app_pets/src/model/rest/appointment.type.dart';
import 'package:app_pets/src/model/rest/pet.list.rs.model.dart';

class AppointmentRq {
  AppointmentRq({
    required this.appointmentType,
    required this.details,
    required this.endDate,
    required this.name,
    required this.pet,
    required this.startDate,
    required this.status,
  });

  final AppointmentType appointmentType;
  final String details;
  final DateTime endDate;
  final String name;
  final Pet pet;
  final DateTime startDate;
  final bool status;

  factory AppointmentRq.fromJson(String str) => AppointmentRq.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory AppointmentRq.fromMap(Map<String, dynamic> json) => AppointmentRq(
        appointmentType: AppointmentType.fromMap(json["appointmentType"]),
        details: json["details"],
        endDate: DateTime.parse(json["endDate"]),
        name: json["name"],
        pet: Pet.fromMap(json["pet"]),
        startDate: DateTime.parse(json["startDate"]),
        status: json["status"],
      );

  Map<String, dynamic> toMap() => {
        "appointmentType": appointmentType.toMap(),
        "details": details,
        "endDate": endDate.toIso8601String(),
        "name": name,
        "pet": pet.toMap(),
        "startDate": startDate.toIso8601String(),
        "status": status,
      };
}
