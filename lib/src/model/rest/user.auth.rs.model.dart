// To parse this JSON data, do
//
//     final userAuthRs = userAuthRsFromMap(jsonString);

import 'dart:convert';

class UserAuthRs {
  UserAuthRs({
    required this.message,
    required this.result,
  });

  final String message;
  final Result result;

  factory UserAuthRs.fromJson(String str) => UserAuthRs.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory UserAuthRs.fromMap(Map<String, dynamic> json) => UserAuthRs(
        message: json["message"],
        result: Result.fromMap(json["result"]),
      );

  Map<String, dynamic> toMap() => {
        "message": message,
        "result": result.toMap(),
      };
}

class Result {
  Result({
    required this.newUser,
    required this.token,
  });

  final bool newUser;
  final String token;

  factory Result.fromJson(String str) => Result.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Result.fromMap(Map<String, dynamic> json) => Result(
        newUser: json["newUser"],
        token: json["token"],
      );

  Map<String, dynamic> toMap() => {
        "newUser": newUser,
        "token": token,
      };
}
