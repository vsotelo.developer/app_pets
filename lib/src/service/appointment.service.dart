import 'package:app_pets/src/helpers/http.interceptor.dart';
import 'package:app_pets/src/helpers/utils.dart';
import 'package:app_pets/src/model/rest/appointment.rq.model.dart';
import 'package:app_pets/src/model/rest/appointment.rs.model.dart';
import 'package:app_pets/src/model/rest/appointment.type.dart';
import 'package:dio/dio.dart';

class AppointmentService {
  final Dio _dioAppointment;

  AppointmentService() : _dioAppointment = Dio()..interceptors.add(HttpInterceptor());

  Future<AppointmentSaveResponse?> registerAppointment(AppointmentRq appointmentRq) async {
    try {
      String url = '$baseUrl/appointment/register/';
      final res = await _dioAppointment.post(url, data: appointmentRq.toJson());
      if (res.statusCode != 200) return null;
      return AppointmentSaveResponse.fromMap(res.data);
    } catch (_) {
      return null;
    }
  }

  Future<AppointmentTypeRs?> getAllAppointmentType() async {
    try {
      String url = '$baseUrl/appointmentType/getAll';
      final res = await _dioAppointment.get(url);
      if (res.statusCode != 200) return null;
      return AppointmentTypeRs.fromMap(res.data);
    } catch (_) {
      return null;
    }
  }
}
