import 'package:app_pets/src/helpers/utils.dart';
import 'package:app_pets/src/model/rest/user.auth.rq.model.dart';
import 'package:app_pets/src/model/rest/user.auth.rs.model.dart';
import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class AuthService {
  final _storage = const FlutterSecureStorage();

  final Dio _dioAuth;

  AuthService() : _dioAuth = Dio();

  // # Read token from secure local storage
  Future<String> readToken() async {
    return await _storage.read(key: 'token') ?? '';
  }

  Future<void> writeToken(String token) async {
    return await _storage.write(key: 'token', value: token);
  }

  Future<String> readNewUser() async {
    return await _storage.read(key: 'newUser') ?? 'false';
  }

  Future<void> writeNewUser(String newUserValue) async {
    return await _storage.write(key: 'newUser', value: newUserValue);
  }

  Future<void> writeFCMToken(String fcmToken) async {
    return await _storage.write(key: 'fcmToken', value: fcmToken);
  }

  
  Future<String> readFCMToken() async {
    return await _storage.read(key: 'fcmToken') ?? '';
  }

  Future<void> removeToken() async {
    return await _storage.delete(key: 'token');
  }

  Future<void> removeNewUser() async {
    return await _storage.delete(key: 'newUser');
  }

  Future<UserAuthRs?> login(UserAuthRq userAuthRq) async {
    try {
      String url = '$baseUrl/auth/user/register/';
      final res = await _dioAuth.post(url, data: userAuthRq.toJson());
      if (res.statusCode != 200) return null;
      return UserAuthRs.fromMap(res.data);
    } catch (_) {
      return null;
    }
  }
}
