import 'package:app_pets/src/helpers/http.interceptor.dart';
import 'package:app_pets/src/helpers/utils.dart';
import 'package:app_pets/src/model/rest/person.rq.model.dart';
import 'package:app_pets/src/model/rest/person.rs.model.dart';
import 'package:dio/dio.dart';

class PersonService {
  final Dio _dioPerson;

  PersonService() : _dioPerson = Dio()..interceptors.add(HttpInterceptor());

  Future<PersonRs?> registerPerson(PersonRq personRq) async {
    try {
      String url = '$baseUrl/person/user/register/';
      final res = await _dioPerson.post(url, data: personRq.toJson());
      if (res.statusCode != 200) return null;
      return PersonRs.fromMap(res.data);
    } catch (_) {
      return null;
    }
  }

  Future<PersonRs?> getPerson() async {
    try {
      String url = '$baseUrl/person/';
      final res = await _dioPerson.get(url);
      if (res.statusCode != 200) return null;
      return PersonRs.fromMap(res.data);
    } catch (_) {
      return null;
    }
  }
}
