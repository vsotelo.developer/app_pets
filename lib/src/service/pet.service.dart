import 'package:app_pets/src/helpers/http.interceptor.dart';
import 'package:app_pets/src/helpers/utils.dart';
import 'package:app_pets/src/model/rest/pet.list.rs.model.dart';
import 'package:app_pets/src/model/rest/pet.rq.model.dart';
import 'package:app_pets/src/model/rest/pet.rs.mode.dart';
import 'package:dio/dio.dart';

class PetService {
  final Dio _dioPet;

  PetService() : _dioPet = Dio()..interceptors.add(HttpInterceptor());

  Future<PetRs?> registerPet(PetRq petRq) async {
    try {
      String url = '$baseUrl/pet/pet/register/';
      final res = await _dioPet.post(url, data: petRq.toJson());
      if (res.statusCode != 200) return null;
      return PetRs.fromMap(res.data);
    } catch (_) {
      return null;
    }
  }

  Future<PetListRs?> getPets() async {
    try {
      String url = '$baseUrl/pet/getAll';
      final res = await _dioPet.get(url);
      if (res.statusCode != 200) return null;
      return PetListRs.fromMap(res.data);
    } catch (_) {
      return null;
    }
  }
}
