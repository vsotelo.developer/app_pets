import 'package:app_pets/main.dart';
import 'package:app_pets/src/bloc/drawer/drawer_bloc.dart';
import 'package:app_pets/src/model/menu.enum.dart';
import 'package:app_pets/src/screens/check.auth.login.dart';
import 'package:app_pets/src/service/auth.service.dart';
import 'package:app_pets/src/theme/ligth.theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sizer/sizer.dart';

// !MENU
class MenuOptionsView extends StatelessWidget {
  const MenuOptionsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final authService = AuthService();
    final drawerBloc = BlocProvider.of<DrawerBloc>(context);
    return BlocBuilder<DrawerBloc, DrawerStateD>(
      builder: (context, state) {
        return SafeArea(
          child: Scaffold(
            backgroundColor: Colors.white,
            body: Container(
              padding: EdgeInsets.only(left: 3.5.h, top: 3.h, bottom: 4.h),
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 10.h,
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          width: 18.w,
                          decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                          ),
                          child: const Image(
                            image: AssetImage('assets/img/avatar_default.png'),
                            fit: BoxFit.fill,
                          ),
                        ),
                        SizedBox(width: 2.w),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Victor Sotelo', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20.sp)),
                            const Text('3 Mascotas', style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal))
                          ],
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 20.h),
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        children: [
                          GestureDetector(
                            onTap: () {
                              drawerBloc.add(const ChangeMenuSelectedEvent(MenuSelected.stepAppointmentPage));
                              zommDrawerController.close!.call();
                            },
                            child: _ItemMenu(
                                label: 'Mis Citas',
                                active: state.menuSelected == MenuSelected.stepAppointmentPage,
                                iconData: Icons.calendar_view_day),
                          ),
                          GestureDetector(
                            onTap: () {
                              drawerBloc.add(const ChangeMenuSelectedEvent(MenuSelected.stepUserPage));
                              zommDrawerController.close!.call();
                            },
                            child: _ItemMenu(
                                label: 'Mi Datos', active: state.menuSelected == MenuSelected.stepUserPage, iconData: Icons.verified_user_outlined),
                          ),
                          GestureDetector(
                            onTap: () {
                              drawerBloc.add(const ChangeMenuSelectedEvent(MenuSelected.stepPetPage));
                              zommDrawerController.close!.call();
                            },
                            child: _ItemMenu(label: 'Mis Mascotas', active: state.menuSelected == MenuSelected.stepPetPage, iconData: Icons.pets),
                          ),
                        ],
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () async {
                      await authService.removeToken();
                      // ignore: use_build_context_synchronously
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => const CheckAuthScreen(),
                        ),
                      );
                    },
                    child: SizedBox(
                      height: 12.h,
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        children: [
                          SizedBox(
                            width: 6.w,
                            child: const Icon(Icons.exit_to_app),
                          ),
                          SizedBox(width: 5.w),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const [
                              Text('AlmaVet', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
                              Text('v 1.0', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold))
                            ],
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}

class _ItemMenu extends StatelessWidget {
  const _ItemMenu({Key? key, required this.label, this.active = false, required this.iconData}) : super(key: key);

  final bool active;
  final String label;
  final IconData iconData;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 8.h,
      child: Row(
        children: [
          Icon(iconData, color: active ? primaryColor : Colors.grey, size: 35),
          const SizedBox(width: 12),
          Text(label, style: TextStyle(color: active ? Colors.black : Colors.grey, fontWeight: FontWeight.bold, fontSize: 12.sp))
        ],
      ),
    );
  }
}
