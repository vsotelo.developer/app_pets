import 'package:app_pets/src/theme/ligth.theme.dart';
import 'package:app_pets/src/widgets/base.app.dart';
import 'package:app_pets/src/widgets/buttom.widget.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class AppointmentDetailScreen extends StatelessWidget {
  const AppointmentDetailScreen({Key? key, required this.date, required this.status}) : super(key: key);

  final String date;
  final String status;

  @override
  Widget build(BuildContext context) {
    Color color = primaryColor.withOpacity(0.2);
    switch (status) {
      case 'DESAPROBADO':
        color = Colors.red.withOpacity(0.2);
        break;
      case 'CANCELADO':
        color = Colors.grey.withOpacity(0.2);
    }

    return SafeArea(
      child: BaseAppWidget(
          accessMenu: const Icon(Icons.arrow_back_ios_new_outlined),
          onPressedAccessMenu: () => Navigator.pop(context),
          scroll: false,
          title: 'Detalle de Cita',
          descriptionTitle: 'Consulta el detalle',
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                date,
                style: TextStyle(fontWeight: FontWeight.w900, fontSize: 16.sp),
              ),
              Text(
                status,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.sp),
              ),
              SizedBox(height: 2.h),
              Container(
                padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
                width: MediaQuery.of(context).size.width,
                height: 80.w,
                decoration: BoxDecoration(
                  color: color,
                  borderRadius: const BorderRadius.all(Radius.circular(35)),
                ),
                child: const FadeInImage(
                  placeholder: AssetImage('assets/img/giphy.gif'),
                  image: NetworkImage('https://api.qrserver.com/v1/create-qr-code/?size=800x800&data=5765678'),
                ),
              ),
              SizedBox(height: 1.h),
              Text(
                'Muestra el siguiente QR al momento de llegar a tu cita.',
                style: TextStyle(fontWeight: FontWeight.normal, fontSize: 12.sp),
              ),
              Text(
                'S/ 50.00',
                style: TextStyle(fontWeight: FontWeight.w900, fontSize: 25.sp),
              ),
              Text(
                'Recuerda acercarte 5 minutos antes de tu cita y traer a tu mascota con su collar o jaula.',
                style: TextStyle(fontWeight: FontWeight.normal, fontSize: 12.sp),
              ),
              SizedBox(height: 2.h),
              CustomButton(
                valueOk: true,
                texto: 'Cacelar Cita',
                onPressed: () {},
              )
            ],
          )),
    );
  }
}
