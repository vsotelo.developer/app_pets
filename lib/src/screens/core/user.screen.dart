import 'package:app_pets/main.dart';
import 'package:app_pets/src/bloc/drawer/drawer_bloc.dart';
import 'package:app_pets/src/model/rest/person.rq.model.dart';
import 'package:app_pets/src/model/rest/person.rs.model.dart';
import 'package:app_pets/src/screens/core/pets.add.dart';
import 'package:app_pets/src/service/auth.service.dart';
import 'package:app_pets/src/service/person.service.dart';
import 'package:app_pets/src/theme/ligth.theme.dart';
import 'package:app_pets/src/widgets/base.app.dart';
import 'package:app_pets/src/widgets/buttom.widget.dart';
import 'package:app_pets/src/widgets/text.imput.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sizer/sizer.dart';

class UserScreen extends StatefulWidget {
  const UserScreen({Key? key, this.isLogin = false}) : super(key: key);

  final bool isLogin;

  @override
  State<UserScreen> createState() => _UserScreenState();
}

class _UserScreenState extends State<UserScreen> {
  bool isSave = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final controllerName = TextEditingController();
    final controllerLastName = TextEditingController();
    final controllerDocumentNumber = TextEditingController();
    final controllerEmail = TextEditingController();
    final controllerPhoneNumber = TextEditingController();

    return BlocBuilder<DrawerBloc, DrawerStateD>(
      builder: (context, state) {
        if (state.personRs != null) {
          controllerName.text = state.personRs!.result.name;
          controllerLastName.text = state.personRs!.result.lastName;
          controllerDocumentNumber.text = state.personRs!.result.documentNumber;
          controllerEmail.text = state.personRs!.result.emailAddress;
          controllerPhoneNumber.text = state.personRs!.result.phoneNumber;
        }
        return SafeArea(
          child: BaseAppWidget(
              onPressedAccessMenu: widget.isLogin ? () {} : () => zommDrawerController.open!.call(),
              accessMenu: widget.isLogin ? const SizedBox() : const Icon(Icons.menu),
              title: 'Mi Información',
              descriptionTitle: widget.isLogin ? 'Completa tu información' : 'Consulta tu información',
              body: Column(
                children: [
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 1, vertical: 10),
                    decoration: BoxDecoration(color: primaryColor.withOpacity(0.2), borderRadius: const BorderRadius.all(Radius.circular(25))),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(width: 2.w),
                        Icon(Icons.security_rounded, color: primaryColor),
                        SizedBox(width: 2.w),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: const [
                            Text('Tus datos son privados y no los'),
                            Text('compartiremos con nadie.'),
                          ],
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 1.h),
                  CustomTextImput(
                    textController: controllerName,
                    hintText: 'Nombres',
                    readOnly: !widget.isLogin,
                  ),
                  CustomTextImput(
                    textController: controllerLastName,
                    hintText: 'Apellidos',
                    readOnly: !widget.isLogin,
                  ),
                  CustomTextImput(
                    textController: controllerDocumentNumber,
                    hintText: 'Numero de Documento',
                    readOnly: !widget.isLogin,
                  ),
                  CustomTextImput(
                    textController: controllerEmail,
                    hintText: 'Email',
                    textInputType: TextInputType.emailAddress,
                    readOnly: !widget.isLogin,
                  ),
                  CustomTextImput(
                    textController: controllerPhoneNumber,
                    hintText: 'Teléfono',
                    textInputType: TextInputType.phone,
                    readOnly: !widget.isLogin,
                  ),
                  SizedBox(height: 3.h),
                  if (widget.isLogin)
                    Column(
                      children: [
                        CustomButton(
                          texto: 'Registrar',
                          valueOk: true,
                          onPressed: () async {
                            isSave = true;
                            setState(() {});

                            PersonRq personRq = PersonRq(
                              documentNumber: controllerDocumentNumber.text,
                              emailAddress: controllerEmail.text,
                              lastName: controllerLastName.text,
                              name: controllerName.text,
                              phoneNumber: controllerPhoneNumber.text,
                            );

                            PersonRs? personRs = await PersonService().registerPerson(personRq);

                            isSave = false;
                            setState(() {});

                            if (personRs != null) {
                              AuthService().writeNewUser('false');

                              isSave = false;
                              setState(() {});

                              Navigator.of(context).push(
                                MaterialPageRoute(
                                  builder: (context) => const PetAddScreen(isLogin: true),
                                ),
                              );
                            }
                          },
                        ),
                        const SizedBox(height: 20),
                        Visibility(
                          visible: isSave,
                          child: CircularProgressIndicator(
                            color: primaryColor,
                            backgroundColor: primaryColor.withOpacity(0.5),
                          ),
                        ),
                      ],
                    )
                ],
              )),
        );
      },
    );
  }
}
