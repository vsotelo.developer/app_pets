import 'package:app_pets/main.dart';
import 'package:app_pets/src/bloc/core/core_bloc.dart';
import 'package:app_pets/src/model/rest/pet.list.rs.model.dart';
import 'package:app_pets/src/screens/core/pets.add.dart';
import 'package:app_pets/src/theme/ligth.theme.dart';
import 'package:app_pets/src/widgets/base.app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sizer/sizer.dart';

class PetsScreen extends StatelessWidget {
  const PetsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CoreBloc, CoreState>(
      builder: (context, state) {
        return BaseAppWidget(
          accessMenu: const Icon(Icons.menu),
          onPressedAccessMenu: () => zommDrawerController.open!.call(),
          title: 'Mis Mascotas',
          descriptionTitle: 'Registra tus engreidos',
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ...state.pets
                  .map(
                    (petRs) => ItemPet(
                      pet: petRs,
                    ),
                  )
                  .toList()
            ],
          ),
          floatingActionButton: FloatingActionButton(
            backgroundColor: primaryColor,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: const [
                Icon(Icons.pets),
                Icon(Icons.add, size: 15),
              ],
            ),
            onPressed: () => Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => const PetAddScreen(),
              ),
            ),
          ),
        );
      },
    );
  }
}

class ItemPet extends StatelessWidget {
  const ItemPet({Key? key, required this.pet}) : super(key: key);

  final Pet pet;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 2.h),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 2.h, vertical: 2.h),
        width: MediaQuery.of(context).size.width,
        height: 10.h,
        decoration: BoxDecoration(
          color: primaryColor.withOpacity(0.2),
          borderRadius: const BorderRadius.all(Radius.circular(25)),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(Icons.pets, color: primaryColor, size: 30),
                Text(
                  pet.gender,
                  style: const TextStyle(fontWeight: FontWeight.bold),
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  pet.name,
                  style: TextStyle(fontWeight: FontWeight.w900, fontSize: 13.sp),
                ),
                Text(pet.specie, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 11.sp), maxLines: 1, overflow: TextOverflow.ellipsis),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('Peso: ${pet.weight} Kilos', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 11.sp)),
                Text('Edad: ${pet.age} años', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 11.sp)),
              ],
            ),
            IconButton(
              icon: const Icon(Icons.remove_circle, color: Colors.red),
              onPressed: () {
                
              },
            ),
          ],
        ),
      ),
    );
  }
}