import 'package:app_pets/src/bloc/core/core_bloc.dart';
import 'package:app_pets/src/model/rest/pet.list.rs.model.dart';
import 'package:app_pets/src/model/rest/pet.rq.model.dart';
import 'package:app_pets/src/model/rest/pet.rs.mode.dart';
import 'package:app_pets/src/screens/main.app.drawer.dart';
import 'package:app_pets/src/service/pet.service.dart';
import 'package:app_pets/src/theme/ligth.theme.dart';
import 'package:app_pets/src/widgets/base.app.dart';
import 'package:app_pets/src/widgets/buttom.widget.dart';
import 'package:app_pets/src/widgets/text.imput.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sizer/sizer.dart';

class PetAddScreen extends StatefulWidget {
  const PetAddScreen({Key? key, this.isLogin = false}) : super(key: key);

  final bool isLogin;

  @override
  State<PetAddScreen> createState() => _PetAddScreenState();
}

class _PetAddScreenState extends State<PetAddScreen> {
  @override
  Widget build(BuildContext context) {
    final coreBloc = BlocProvider.of<CoreBloc>(context);

    final controllerName = TextEditingController();
    final controllerAge = TextEditingController();
    final controllerPeso = TextEditingController();
    final controllerRaza = TextEditingController();
    final controllerEspecie = TextEditingController();

    List<String> generos = ["Machito", "Hembrita"];

    bool isSave = false;

    return BlocBuilder<CoreBloc, CoreState>(
      builder: (context, state) {
        return SafeArea(
          child: BaseAppWidget(
            onPressedAccessMenu: widget.isLogin ? () {} : () => Navigator.pop(context),
            accessMenu: widget.isLogin ? const SizedBox() : const Icon(Icons.arrow_back_ios_new_outlined),
            title: 'Registra tu Mascota',
            descriptionTitle: 'Completa los datos de tu \nengreido',
            body: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomTextImput(
                  textController: controllerName,
                  hintText: 'Nombre *',
                ),
                CustomTextImput(
                  textController: controllerAge,
                  hintText: 'Edad (Años humanos)',
                  textInputType: TextInputType.number,
                ),
                CustomTextImput(
                  textController: controllerPeso,
                  hintText: 'Peso (Kilos)',
                  textInputType: TextInputType.number,
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 4.w),
                  decoration: BoxDecoration(
                    border: Border.all(color: primaryColor, width: 2.5),
                    borderRadius: const BorderRadius.all(Radius.circular(30)),
                  ),
                  child: DropdownButton(
                    borderRadius: BorderRadius.circular(30),
                    iconEnabledColor: primaryColor,
                    underline: const SizedBox(),
                    isExpanded: true,
                    hint: Text(state.genderSelected != "" ? state.genderSelected : 'Seleccione Genero - (M o H)',
                        style: TextStyle(fontWeight: FontWeight.bold, color: primaryColor)),
                    items: generos
                        .map<DropdownMenuItem<String>>((genero) => DropdownMenuItem<String>(
                              value: genero,
                              child: Text(genero, style: TextStyle(fontWeight: FontWeight.bold, color: primaryColor)),
                            ))
                        .toList(),
                    onChanged: (String? genero) {
                      if (genero != null) {
                        coreBloc.add(OnChangeGenderSelected(genero));
                      }
                    },
                  ),
                ),
                CustomTextImput(
                  textController: controllerRaza,
                  hintText: 'Raza',
                ),
                CustomTextImput(
                  textController: controllerEspecie,
                  hintText: 'Especie',
                ),
                SizedBox(height: 2.h),
                Visibility(
                  visible: isSave,
                  child: Center(
                    child: CircularProgressIndicator(
                      color: primaryColor,
                      backgroundColor: primaryColor.withOpacity(0.5),
                    ),
                  ),
                ),
                SizedBox(height: 2.h),
                Visibility(
                  visible: !state.registerOkPet,
                  child: Text('(*) Existen errores en el registro,', style: TextStyle(fontWeight: FontWeight.bold, color: primaryColor)),
                ),
                SizedBox(height: 2.h),
                CustomButton(
                  texto: 'Registrar',
                  valueOk: true,
                  onPressed: () async {
                    isSave = true;
                    setState(() {});

                    if (controllerName.text.trim() == '') {
                      coreBloc.add(const OnSetValueOkRegisterPetEvent(false));
                      return;
                    }

                    if (state.genderSelected.trim() == '') {
                      coreBloc.add(const OnSetValueOkRegisterPetEvent(false));
                      return;
                    }

                    coreBloc.add(const OnSetValueOkRegisterPetEvent(true));

                    PetRq petRq = PetRq(
                      age: int.parse(controllerAge.text),
                      breed: controllerRaza.text,
                      gender: state.genderSelected,
                      name: controllerName.text,
                      specie: controllerEspecie.text,
                      weight: int.parse(controllerPeso.text),
                    );

                    PetRs? petRs = await PetService().registerPet(petRq);

                    isSave = false;
                    setState(() {});

                    if (petRs != null) {
                      PetListRs? petListRs = await PetService().getPets();
                      if (petListRs != null) {
                        coreBloc.add(OnSetPetsEvent(petListRs.result));
                      }
                      Navigator.pop(context);
                    }
                  },
                ),
                SizedBox(height: 2.h),
                if (widget.isLogin)
                  CustomButton(
                    texto: 'Registrar más tarde',
                    valueOk: true,
                    onPressed: () => Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => const MainAppDrawer(),
                      ),
                    ),
                  )
              ],
            ),
          ),
        );
      },
    );
  }
}
