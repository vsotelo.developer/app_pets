import 'package:app_pets/src/bloc/core/core_bloc.dart';
import 'package:app_pets/src/model/rest/appointment.rq.model.dart';
import 'package:app_pets/src/model/rest/appointment.type.dart';
import 'package:app_pets/src/model/rest/pet.list.rs.model.dart';
import 'package:app_pets/src/service/appointment.service.dart';
import 'package:app_pets/src/theme/ligth.theme.dart';
import 'package:app_pets/src/widgets/base.app.dart';
import 'package:app_pets/src/widgets/buttom.widget.dart';
import 'package:app_pets/src/widgets/custom.text.imput.area.dart';
import 'package:app_pets/src/widgets/text.imput.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sizer/sizer.dart';

class AppointmentsAddScreen extends StatefulWidget {
  const AppointmentsAddScreen({Key? key, this.restorationId}) : super(key: key);

  final String? restorationId;

  @override
  State<AppointmentsAddScreen> createState() => _AppointmentsAddScreenState();
}

class _AppointmentsAddScreenState extends State<AppointmentsAddScreen> with RestorationMixin {
  void _selectDate(DateTime? newSelectedDate) {
    final coreBloc = BlocProvider.of<CoreBloc>(context);
    if (newSelectedDate != null) {
      _selectedDate.value = newSelectedDate;
      coreBloc.add(OnSetNewDateSelected('${_selectedDate.value.day}/${_selectedDate.value.month}/${_selectedDate.value.year}'));
      coreBloc.add(OnSetDateSelectedDatetimeEvent(newSelectedDate));
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    final coreBloc = BlocProvider.of<CoreBloc>(context);

    final controllerDetail = TextEditingController();

    return BlocBuilder<CoreBloc, CoreState>(
      builder: (context, state) {
        return SafeArea(
          child: BaseAppWidget(
            accessMenu: const Icon(Icons.arrow_back_ios_new_outlined),
            onPressedAccessMenu: () => Navigator.pop(context),
            title: 'Registro de Citas',
            descriptionTitle: 'Completa los datos',
            body: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 4.w),
                  decoration: BoxDecoration(
                    border: Border.all(color: primaryColor, width: 2.5),
                    borderRadius: const BorderRadius.all(Radius.circular(30)),
                  ),
                  child: DropdownButton(
                    borderRadius: BorderRadius.circular(30),
                    iconEnabledColor: primaryColor,
                    underline: const SizedBox(),
                    isExpanded: true,
                    hint: Text(state.appointmentTypeSelected != null ? state.appointmentTypeSelected!.name : 'Seleccione Tipo de Cita',
                        style: TextStyle(fontWeight: FontWeight.bold, color: primaryColor)),
                    items: state.appointmentTypes
                        .map<DropdownMenuItem<AppointmentType>>((appointmentType) => DropdownMenuItem<AppointmentType>(
                              value: appointmentType,
                              child: Text(appointmentType.name, style: const TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
                            ))
                        .toList(),
                    onChanged: (AppointmentType? appointmentType) {
                      if (appointmentType != null) {
                        coreBloc.add(OnChangeAppointmentTypeEvent(appointmentType));
                      }
                    },
                  ),
                ),
                SizedBox(height: 1.h),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 4.w),
                  decoration: BoxDecoration(
                    border: Border.all(color: primaryColor, width: 2.5),
                    borderRadius: const BorderRadius.all(Radius.circular(30)),
                  ),
                  child: DropdownButton(
                    borderRadius: BorderRadius.circular(30),
                    iconEnabledColor: primaryColor,
                    underline: const SizedBox(),
                    isExpanded: true,
                    hint: Text(state.petSelected != null ? state.petSelected!.name : 'Seleccione Mascota',
                        style: const TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
                    items: state.pets
                        .map<DropdownMenuItem<Pet>>((pet) => DropdownMenuItem<Pet>(
                              value: pet,
                              child: Text(pet.name, style: const TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
                            ))
                        .toList(),
                    onChanged: (Pet? pet) {
                      if (pet != null) {
                        coreBloc.add(OnChangePetEvent(pet));
                      }
                    },
                  ),
                ),
                SizedBox(height: 1.h),
                CustomTextImputArea(
                  textController: controllerDetail,
                  hintText: 'Detalle',
                ),
                SizedBox(height: 1.h),
                Divider(color: primaryColor),
                GestureDetector(
                  child: CustomTextImput(
                    readOnly: true,
                    textController: state.dateSelected != '' ? (TextEditingController()..text = '2022/01/01') : TextEditingController()
                      ..text = state.dateSelected,
                    hintText: 'Fecha:',
                  ),
                  onTap: () {
                    _restorableDatePickerRouteFuture.present();
                  },
                ),
                SizedBox(height: 1.h),
                Divider(color: primaryColor),
                Text('Hora', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 13.sp)),
                if (state.appointmentTypeSelected != null)
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 4.w),
                    decoration: BoxDecoration(
                      border: Border.all(color: primaryColor, width: 2.5),
                      borderRadius: const BorderRadius.all(Radius.circular(30)),
                    ),
                    child: DropdownButton(
                      borderRadius: BorderRadius.circular(30),
                      iconEnabledColor: primaryColor,
                      underline: const SizedBox(),
                      isExpanded: true,
                      hint: Text(state.appointmentTypeSelected != null ? state.appointmentTypeSelected!.name : 'Seleccione Tipo de Cita',
                          style: TextStyle(fontWeight: FontWeight.bold, color: primaryColor)),
                      items: state.appointmentTypeSelected!.scheduleList[0].hours
                          .map<DropdownMenuItem<int>>((hour) => DropdownMenuItem<int>(
                                value: hour,
                                child: Text('$hour:00 Horas', style: const TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
                              ))
                          .toList(),
                      onChanged: (int? hour) {
                        if (hour != null) {
                          //coreBloc.add(OnChangeAppointmentTypeEvent(appointmentType));
                        }
                      },
                    ),
                  ),
                Visibility(
                  visible: state.registerOkAppointment,
                  child: Text('(*) Existen errores en el registro,', style: TextStyle(fontWeight: FontWeight.bold, color: primaryColor)),
                ),
                SizedBox(height: 1.h),
                CustomButton(
                  texto: 'Registrar',
                  valueOk: state.registerOkAppointment,
                  onPressed: () async {
                    if (state.appointmentTypeSelected != null) {
                      coreBloc.add(const OnSetValueOkRegisterAppointmentEvent(true));
                      return;
                    }
                    if (state.petSelected != null) {
                      coreBloc.add(const OnSetValueOkRegisterAppointmentEvent(true));
                      return;
                    }
                    if (state.datetimeSelected != null) {
                      coreBloc.add(const OnSetValueOkRegisterAppointmentEvent(true));
                      return;
                    }                    

                    AppointmentRq appointmentRq = AppointmentRq(
                      appointmentType: state.appointmentTypeSelected!,
                      pet: state.petSelected!,
                      details: controllerDetail.text,                      
                      name: state.petSelected!.name,
                      status: true,
                      startDate: state.datetimeSelected!,
                      endDate: state.datetimeSelected!,
                    );

                    AppointmentService().registerAppointment(appointmentRq);
                  },
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  String? get restorationId => widget.restorationId;

  final RestorableDateTime _selectedDate = RestorableDateTime(DateTime.now());
  late final RestorableRouteFuture<DateTime?> _restorableDatePickerRouteFuture = RestorableRouteFuture<DateTime?>(
    onComplete: _selectDate,
    onPresent: (NavigatorState navigator, Object? arguments) {
      return navigator.restorablePush(
        _datePickerRoute,
        arguments: _selectedDate.value.millisecondsSinceEpoch,
      );
    },
  );

  static Route<DateTime> _datePickerRoute(
    BuildContext context,
    Object? arguments,
  ) {
    return DialogRoute<DateTime>(
      context: context,
      builder: (BuildContext context) {
        return DatePickerDialog(
          restorationId: 'date_picker_dialog',
          initialEntryMode: DatePickerEntryMode.calendarOnly,
          initialDate: DateTime.fromMillisecondsSinceEpoch(arguments! as int),
          firstDate: DateTime(1994),
          lastDate: DateTime(2050),
        );
      },
    );
  }

  @override
  void restoreState(RestorationBucket? oldBucket, bool initialRestore) {
    registerForRestoration(_selectedDate, 'selected_date');
    registerForRestoration(_restorableDatePickerRouteFuture, 'date_picker_route_future');
  }
}
