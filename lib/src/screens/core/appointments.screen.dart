import 'package:app_pets/main.dart';
import 'package:app_pets/src/screens/core/appointments.add.screen.dart';
import 'package:app_pets/src/screens/core/appointments.detail.screen.dart';
import 'package:app_pets/src/theme/ligth.theme.dart';
import 'package:app_pets/src/widgets/base.app.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class AppointmentsScreen extends StatelessWidget {
  const AppointmentsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {        
    return BaseAppWidget(
      accessMenu: const Icon(Icons.menu, size: 33),
      onPressedAccessMenu:  () => zommDrawerController.open!.call(),
      title: 'Mis Citas',
      descriptionTitle: 'Consulta tus citas',
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 1, vertical: 10),
            decoration: BoxDecoration(
              color: primaryColor.withOpacity(0.2),
              borderRadius: const BorderRadius.all(Radius.circular(25)),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(width: 2.w),
                Icon(Icons.info, color: primaryColor),
                SizedBox(width: 2.w),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: const [
                    Text('No te olvides de acercarte 5 minutos antes,'),
                    Text('con tu dni y tu mascota con su collar.'),
                  ],
                )
              ],
            ),
          ),
          SizedBox(height: 2.h),
          ...appointments
              .map(
                (appointment) => ItemAppointment(
                  appointment: appointment,
                ),
              )
              .toList()
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: primaryColor,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: const [
            Icon(Icons.assignment),
            Icon(Icons.add, size: 15),
          ],
        ),
        onPressed: () => Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => const AppointmentsAddScreen(),
          ),
        ),
      ),
    );
  }
}

class ItemAppointment extends StatelessWidget {
  const ItemAppointment({Key? key, required this.appointment}) : super(key: key);

  final AppointmentLab appointment;

  @override
  Widget build(BuildContext context) {
    Color color = primaryColor.withOpacity(0.1);
    switch (appointment.status) {
      case 'DESAPROBADO':
        color = Colors.red.withOpacity(0.4);
        break;
      case 'CANCELADO':
        color = Colors.grey.withOpacity(0.2);
    }

    return Padding(
      padding: EdgeInsets.only(bottom: 2.h),
      child: GestureDetector(
        onTap: () => Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => AppointmentDetailScreen(
              date: appointment.date,
              status: appointment.status,
            ),
          ),
        ),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 2.h, vertical: 2.h),
          width: MediaQuery.of(context).size.width,
          height: 10.h,
          decoration: BoxDecoration(
            color: color,
            borderRadius: const BorderRadius.all(Radius.circular(25)),
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Icon(Icons.assignment, color: primaryColor, size: 30),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    appointment.date,
                    style: TextStyle(fontWeight: FontWeight.w900, fontSize: 12.sp),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(Icons.timer, size: 15, color: primaryColor),
                      const SizedBox(width: 5),
                      Text('12:00 PM', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 11.sp)),
                      SizedBox(width: 5.w),
                      Icon(Icons.space_dashboard_sharp, size: 15, color: primaryColor),
                      const SizedBox(width: 5),
                      Text(appointment.status, style: TextStyle(fontWeight: FontWeight.w900, fontSize: 10.sp)),
                    ],
                  ),
                ],
              ),
              IconButton(
                icon: Icon(Icons.arrow_forward_ios_rounded, color: primaryColor),
                onPressed: () => Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => AppointmentDetailScreen(
                      date: appointment.date,
                      status: appointment.status,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

List<AppointmentLab> appointments = [
  AppointmentLab(date: 'Lunes 31 de Mayo', status: 'APROBADO', category: 'Baño', price: 'S/ 50.00'),
  AppointmentLab(date: 'Lunes 31 de Mayo', status: 'DESAPROBADO', category: 'Baño', price: 'S/ 50.00'),
  AppointmentLab(date: 'Lunes 31 de Mayo', status: 'APROBADO', category: 'Baño', price: 'S/ 50.00'),
  AppointmentLab(date: 'Lunes 31 de Mayo', status: 'CANCELADO', category: 'Baño', price: 'S/ 50.00'),
  AppointmentLab(date: 'Lunes 31 de Mayo', status: 'DESAPROBADO', category: 'Baño', price: 'S/ 50.00'),
  AppointmentLab(date: 'Lunes 31 de Mayo', status: 'APROBADO', category: 'Baño', price: 'S/ 50.00'),
  AppointmentLab(date: 'Lunes 31 de Mayo', status: 'CANCELADO', category: 'Baño', price: 'S/ 50.00'),
  AppointmentLab(date: 'Lunes 31 de Mayo', status: 'APROBADO', category: 'Baño', price: 'S/ 50.00'),
  AppointmentLab(date: 'Lunes 31 de Mayo', status: 'DESAPROBADO', category: 'Baño', price: 'S/ 50.00'),
  AppointmentLab(date: 'Lunes 31 de Mayo', status: 'APROBADO', category: 'Baño', price: 'S/ 50.00'),
  AppointmentLab(date: 'Lunes 31 de Mayo', status: 'APROBADO', category: 'Baño', price: 'S/ 50.00'),
  AppointmentLab(date: 'Lunes 31 de Mayo', status: 'APROBADO', category: 'Baño', price: 'S/ 50.00'),
  AppointmentLab(date: 'Lunes 31 de Mayo', status: 'DESAPROBADO', category: 'Baño', price: 'S/ 50.00'),
];

class AppointmentLab {
  AppointmentLab({
    required this.date,
    required this.status,
    required this.category,
    required this.price,
  });
  final String date;
  final String status;
  final String category;
  final String price;
}
