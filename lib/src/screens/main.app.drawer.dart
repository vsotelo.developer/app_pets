import 'package:app_pets/main.dart';
import 'package:app_pets/src/bloc/drawer/drawer_bloc.dart';
import 'package:app_pets/src/model/menu.enum.dart';
import 'package:app_pets/src/screens/core/appointments.screen.dart';
import 'package:app_pets/src/screens/core/pets.screen.dart';
import 'package:app_pets/src/screens/core/user.screen.dart';
import 'package:app_pets/src/screens/menu.screem.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_zoom_drawer/flutter_zoom_drawer.dart';
import 'package:sizer/sizer.dart';

import '../theme/ligth.theme.dart';

class MainAppDrawer extends StatelessWidget {
  const MainAppDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DrawerBloc, DrawerStateD>(
      builder: (context, state) {
        Widget mainScreen;
        switch(state.menuSelected){          
          case MenuSelected.stepUserPage:            
            mainScreen = const UserScreen();
            break;
          case MenuSelected.stepPetPage:
            mainScreen = const PetsScreen();
            break;
          case MenuSelected.stepAppointmentPage:
            mainScreen = const AppointmentsScreen();
            break;          
        }
        return SafeArea(          
          child: ZoomDrawer(
            controller: zommDrawerController,
            angle: 0,
            menuScreenWidth: 69.w,
            menuBackgroundColor: Colors.white,
            menuScreen: const MenuOptionsView(),
            mainScreen: mainScreen,
            shadowLayer1Color: primaryColor.withOpacity(0.5),
            shadowLayer2Color: primaryColor.withOpacity(0.8),
            showShadow: true,
          ),
        );
      },
    );
  }
}
