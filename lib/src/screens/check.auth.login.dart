import 'package:app_pets/src/screens/core/user.screen.dart';
import 'package:app_pets/src/screens/login.screen.dart';
import 'package:app_pets/src/screens/main.app.drawer.dart';
import 'package:app_pets/src/service/auth.service.dart';
import 'package:app_pets/src/theme/ligth.theme.dart';
import 'package:flutter/material.dart';

class CheckAuthScreen extends StatefulWidget {
  const CheckAuthScreen({Key? key}) : super(key: key);

  @override
  State<CheckAuthScreen> createState() => _CheckAuthScreenState();
}

class _CheckAuthScreenState extends State<CheckAuthScreen> {
  late Image image;
  late Image image2;

  @override
  void initState() {
    super.initState();
    image = Image.asset('assets/img/logo.png');
    image2 = Image.asset('assets/img/background.jpg');
  }

  @override
  void didChangeDependencies() {
    precacheImage(image.image, context);
    precacheImage(image2.image, context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final authService = AuthService();

    return FutureBuilder(
        future: authService.readToken(),
        builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
          if (!snapshot.hasData) return const _AwaitReadTokenScreenOne();

          if (snapshot.data == '') {
            Future.microtask(() {
              Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) => const LoginScreen()));
            });
          } else {
            Future.microtask(() async {
              String newUser = await authService.readNewUser();
              if (newUser == 'true') {
                // ignore: use_build_context_synchronously
                Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) => const UserScreen(isLogin: true)));                
              } else {
                // ignore: use_build_context_synchronously
                Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) => const MainAppDrawer()));
              }
            });
          }

          return const _AwaitReadTokenScreenOne();
        });
  }
}

class _AwaitReadTokenScreenOne extends StatelessWidget {
  const _AwaitReadTokenScreenOne({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Center(
          child: CircularProgressIndicator(
            color: primaryColor,
            backgroundColor: primaryColor.withOpacity(0.5),
          ),
        ),
      ),
    );
  }
}
