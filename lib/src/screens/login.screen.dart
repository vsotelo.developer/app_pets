import 'package:animate_do/animate_do.dart';
import 'package:app_pets/src/bloc/login/login_bloc.dart';
import 'package:app_pets/src/model/rest/user.auth.rq.model.dart';
import 'package:app_pets/src/model/rest/user.auth.rs.model.dart';
import 'package:app_pets/src/screens/core/user.screen.dart';
import 'package:app_pets/src/screens/main.app.drawer.dart';
import 'package:app_pets/src/service/auth.service.dart';
import 'package:app_pets/src/theme/ligth.theme.dart';
import 'package:app_pets/src/widgets/buttom.widget.dart';
import 'package:app_pets/src/widgets/text.imput.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sizer/sizer.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final loginBloc = BlocProvider.of<LoginBloc>(context);
    final authService = AuthService();
    final controllerUsername = TextEditingController();
    final controllerPassword = TextEditingController();

    return Scaffold(
      body: BlocBuilder<LoginBloc, LoginState>(
        builder: (context, state) {
          return SizedBox(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Stack(
              children: [
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  child: const Image(image: AssetImage('assets/img/background.jpg'), fit: BoxFit.fill),
                ),
                Positioned(
                  bottom: 0,
                  child: Column(
                    children: [
                      ElasticIn(
                        child: const SizedBox(
                          width: 150,
                          child: Image(image: AssetImage('assets/img/logo.png'), fit: BoxFit.fill),
                        ),
                      ),
                      const SizedBox(height: 20),
                      Visibility(
                        visible: state.isLoginLoanding,
                        child: CircularProgressIndicator(
                          color: primaryColor,
                          backgroundColor: primaryColor.withOpacity(0.5),
                        ),
                      ),
                      SizedBox(height: 3.h),
                      FadeInUp(
                        child: Container(
                          padding: EdgeInsets.only(left: 3.h, right: 3.h, top: 4.h),
                          width: MediaQuery.of(context).size.width,
                          height: 47.h,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: const BorderRadius.only(topLeft: Radius.circular(30), topRight: Radius.circular(30)),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black.withOpacity(0.3),
                                blurRadius: 9,
                                offset: const Offset(0, -5),
                              ),
                            ],
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Bienvenido', style: TextStyle(fontWeight: FontWeight.w900, fontSize: 22.sp)),
                              Text('Ingrese sus credenciales para ingresar.', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.sp)),
                              SizedBox(height: 2.h),
                              CustomTextImput(textController: controllerUsername, hintText: 'Usuario'),
                              CustomTextImput(textController: controllerPassword, hintText: 'Contraseña', obscureText: true),
                              Visibility(
                                visible: state.isPasswordError,
                                child: Transform.translate(
                                  offset: const Offset(20, -5),
                                  child: Text('Contraseña Incorrecta',
                                      style: TextStyle(fontWeight: FontWeight.bold, color: primaryColor.withOpacity(0.8))),
                                ),
                              ),
                              SizedBox(height: 1.h),
                              CustomButton(
                                texto: 'Ingresar',
                                valueOk: true,
                                onPressed: () async {
                                  loginBloc.add(const IsLoginLoandingEvent(true));
                                  
                                  UserAuthRq userAuthRq = UserAuthRq(
                                    password: controllerPassword.text,
                                    username: controllerUsername.text,
                                    tokenFcm: await authService.readFCMToken()
                                  );
                                  UserAuthRs? userAuthRs = await authService.login(userAuthRq);

                                  if (userAuthRs == null) {
                                    loginBloc.add(const IsLoginPasswordErrorEvent(true));
                                  } else {
                                    loginBloc.add(const IsLoginPasswordErrorEvent(false));
                                    authService.writeToken(userAuthRs.result.token);

                                    loginBloc.add(const IsLoginLoandingEvent(false));

                                    if (userAuthRs.result.newUser) {
                                      authService.writeNewUser('true');
                                      // ignore: use_build_context_synchronously
                                      Navigator.of(context).push(
                                        MaterialPageRoute(
                                          builder: (context) => const UserScreen(isLogin: true),
                                        ),
                                      );
                                    } else {
                                      authService.writeNewUser('false');
                                      // ignore: use_build_context_synchronously
                                      Navigator.of(context).pushReplacement(
                                        MaterialPageRoute(
                                          builder: (context) => const MainAppDrawer(),
                                        ),
                                      );
                                    }
                                  }
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
