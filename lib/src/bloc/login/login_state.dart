part of 'login_bloc.dart';

class LoginState extends Equatable {
  const LoginState({this.isLoginLoanding = false, this.isPasswordError = false});

  final bool isLoginLoanding;
  final bool isPasswordError;

  LoginState copyWith({
    bool? isLoginLoanding,
    bool? isPasswordError,
  }) =>
  LoginState(
    isLoginLoanding: isLoginLoanding ?? this.isLoginLoanding,
    isPasswordError: isPasswordError ?? this.isPasswordError,
  );

  @override
  List<Object> get props => [isLoginLoanding, isPasswordError];
}
