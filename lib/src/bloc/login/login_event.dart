part of 'login_bloc.dart';

abstract class LoginEvent extends Equatable {
  const LoginEvent();

  @override
  List<Object> get props => [];
}

class IsLoginLoandingEvent extends LoginEvent{
  const IsLoginLoandingEvent(this.isLoginLoanding);
  final bool isLoginLoanding;
}

class IsLoginPasswordErrorEvent extends LoginEvent{
  const IsLoginPasswordErrorEvent(this.isPasswordError);
  final bool isPasswordError;
}
