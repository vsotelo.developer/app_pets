import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc() : super(const LoginState()) {
    on<LoginEvent>((event, emit) {});
    on<IsLoginLoandingEvent>((event, emit) => emit(state.copyWith(isLoginLoanding: event.isLoginLoanding)));
    on<IsLoginPasswordErrorEvent>((event, emit) => emit(state.copyWith(isPasswordError: event.isPasswordError)));
  }
}
