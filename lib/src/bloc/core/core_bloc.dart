import 'package:app_pets/src/model/rest/appointment.type.dart';
import 'package:app_pets/src/model/rest/pet.list.rs.model.dart';
import 'package:app_pets/src/service/appointment.service.dart';
import 'package:app_pets/src/service/pet.service.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'core_event.dart';
part 'core_state.dart';

class CoreBloc extends Bloc<CoreEvent, CoreState> {
  CoreBloc() : super(const CoreState()) {
    on<CoreEvent>((event, emit) {});
    on<OnSetPetsEvent>((event, emit) => emit(state.copyWith(pets: event.pets)));
    on<OnSetAppointmentTypesEvent>((event, emit) => emit(state.copyWith(appointmentTypes: event.appointmentTypes)));
    on<OnChangeAppointmentTypeEvent>((event, emit) => emit(state.copyWith(appointmentTypeSelected: event.appointmentTypeSelected)));
    on<OnChangePetEvent>((event, emit) => emit(state.copyWith(petSelected: event.petSelected)));
    on<OnSetValueOkRegisterAppointmentEvent>((event, emit) => emit(state.copyWith(registerOkAppointment: event.registerOkAppointment)));
    on<OnChangeGenderSelected>((event, emit) => emit(state.copyWith(genderSelected: event.genderSelected)));
    on<OnSetValueOkRegisterPetEvent>((event, emit) => emit(state.copyWith(registerOkPet: event.registerOkPet)));
    on<OnSetNewDateSelected>((event, emit) => emit(state.copyWith(dateSelected: event.dateSelected)));
    on<OnSetDateSelectedDatetimeEvent>((event, emit) => state.copyWith(datetimeSelected: event.datetimeSelected));
    _init();
  }

  _init() async {
    PetListRs? petListRs = await PetService().getPets();
    if (petListRs != null) {
      add(OnSetPetsEvent(petListRs.result));
    }
    AppointmentTypeRs? appointmentTypeRs = await AppointmentService().getAllAppointmentType();
    if (appointmentTypeRs != null) {
      add(OnSetAppointmentTypesEvent(appointmentTypeRs.result));
    }
  }
}
