part of 'core_bloc.dart';

class CoreState extends Equatable {
  const CoreState({
    this.pets = const [],
    this.appointmentTypes = const [],
    this.appointmentTypeSelected,
    this.petSelected,
    this.registerOkAppointment = true,
    this.genderSelected = '',
    this.registerOkPet = true,
    this.dateSelected = '',
    this.datetimeSelected,
  });

  final List<Pet> pets;
  final List<AppointmentType> appointmentTypes;
  final AppointmentType? appointmentTypeSelected;
  final Pet? petSelected;
  final bool registerOkAppointment;
  final String genderSelected;
  final bool registerOkPet;
  final String dateSelected;    
  final DateTime? datetimeSelected;

  CoreState copyWith({
    List<Pet>? pets,
    List<AppointmentType>? appointmentTypes,
    AppointmentType? appointmentTypeSelected,
    Pet? petSelected,
    bool? registerOkAppointment,
    String? genderSelected,
    bool? registerOkPet,
    String? dateSelected,
    DateTime? datetimeSelected,
  }) =>
      CoreState(
        pets: pets ?? this.pets,
        appointmentTypes: appointmentTypes ?? this.appointmentTypes,
        appointmentTypeSelected: appointmentTypeSelected ?? this.appointmentTypeSelected,
        petSelected: petSelected ?? this.petSelected,
        registerOkAppointment: registerOkAppointment ?? this.registerOkAppointment,
        genderSelected: genderSelected ?? this.genderSelected,
        registerOkPet : registerOkPet ?? this.registerOkPet,
        dateSelected: dateSelected ?? this.dateSelected,
        datetimeSelected: datetimeSelected ?? this.datetimeSelected,
      );

  @override
  List<Object?> get props => [pets, appointmentTypes, appointmentTypeSelected, petSelected, registerOkAppointment, genderSelected, registerOkPet, dateSelected];
}
