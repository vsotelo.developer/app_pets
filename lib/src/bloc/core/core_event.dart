part of 'core_bloc.dart';

abstract class CoreEvent extends Equatable {
  const CoreEvent();

  @override
  List<Object> get props => [];
}

class OnSetPetsEvent extends CoreEvent {
  const OnSetPetsEvent(this.pets);
  final List<Pet> pets;  
}

class OnSetAppointmentTypesEvent extends CoreEvent {
  const OnSetAppointmentTypesEvent(this.appointmentTypes);
  final List<AppointmentType> appointmentTypes;  
}

class OnChangeAppointmentTypeEvent extends CoreEvent {
  const OnChangeAppointmentTypeEvent(this.appointmentTypeSelected); 
  final AppointmentType appointmentTypeSelected;  
}

class OnChangePetEvent extends CoreEvent {
  const OnChangePetEvent(this.petSelected); 
  final Pet petSelected;  
}

class OnSetValueOkRegisterAppointmentEvent extends CoreEvent {
   const OnSetValueOkRegisterAppointmentEvent(this.registerOkAppointment);
   final bool registerOkAppointment;
}

class OnChangeGenderSelected extends CoreEvent {
  const OnChangeGenderSelected(this.genderSelected);
  final String genderSelected;  
}

class OnSetValueOkRegisterPetEvent extends CoreEvent {
  const OnSetValueOkRegisterPetEvent(this.registerOkPet);
  final bool registerOkPet;
}

class OnSetNewDateSelected extends CoreEvent {
  const OnSetNewDateSelected(this.dateSelected);
  final String dateSelected;  
}

class OnSetDateSelectedDatetimeEvent extends CoreEvent {
  const OnSetDateSelectedDatetimeEvent(this.datetimeSelected);
  final DateTime datetimeSelected;  
}