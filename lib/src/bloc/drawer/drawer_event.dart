part of 'drawer_bloc.dart';

abstract class DrawerEvent extends Equatable {
  const DrawerEvent();

  @override
  List<Object> get props => [];
}

class ChangeMenuSelectedEvent extends DrawerEvent {
  const ChangeMenuSelectedEvent(this.menuSelected);
  final MenuSelected menuSelected;
}

class OnSetPersonDataUserRs extends DrawerEvent {
  const OnSetPersonDataUserRs(this.personRs);
  final PersonRs personRs;  
}