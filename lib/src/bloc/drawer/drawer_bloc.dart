import 'package:app_pets/src/model/menu.enum.dart';
import 'package:app_pets/src/model/rest/person.rs.model.dart';
import 'package:app_pets/src/service/person.service.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'drawer_event.dart';
part 'drawer_state.dart';

class DrawerBloc extends Bloc<DrawerEvent, DrawerStateD> {
  DrawerBloc() : super(const DrawerStateD()) {
    on<DrawerEvent>((event, emit) {});
    on<ChangeMenuSelectedEvent>((event, emit) => emit(state.copyWith(menuSelected: event.menuSelected)));
    on<OnSetPersonDataUserRs>((event, emit) => emit(state.copyWith(personRs: event.personRs)));
    _init();
  }

  _init() async {
    PersonRs? personRs = await PersonService().getPerson();
    if (personRs != null) {
      add(OnSetPersonDataUserRs(personRs));
    }
  }
}
