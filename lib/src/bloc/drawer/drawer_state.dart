part of 'drawer_bloc.dart';

class DrawerStateD extends Equatable {
  const DrawerStateD({
    this.menuSelected = MenuSelected.stepAppointmentPage,
    this.personRs,
  });

  final MenuSelected menuSelected;
  final PersonRs? personRs;

  DrawerStateD copyWith({
    MenuSelected? menuSelected,
    PersonRs? personRs,
  }) =>
      DrawerStateD(
        menuSelected: menuSelected ?? this.menuSelected,
        personRs: personRs ?? this.personRs,
      );

  @override
  List<Object?> get props => [menuSelected, personRs];
}
