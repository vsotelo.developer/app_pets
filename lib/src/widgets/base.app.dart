import 'package:animate_do/animate_do.dart';
import 'package:app_pets/main.dart';
import 'package:app_pets/src/theme/ligth.theme.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class BaseAppWidget extends StatelessWidget {
  const BaseAppWidget({
    Key? key,
    required this.title,
    required this.descriptionTitle,
    this.showIconAction = false,
    required this.body,
    this.floatingActionButton,
    this.scroll = true,
    required this.accessMenu,
    this.onPressedAccessMenu,
  }) : super(key: key);

  final String title;
  final String descriptionTitle;
  final bool showIconAction;
  final Widget body;
  final Widget? floatingActionButton;
  final bool scroll;
  final Widget accessMenu;
  final VoidCallback? onPressedAccessMenu;

  @override
  Widget build(BuildContext context) {
    return Scaffold(      
      floatingActionButton: floatingActionButton,      
      body: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Stack(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              color: primaryColor,
              child: Stack(
                children: [
                  Positioned(
                    top: 1.h,
                    left: 5.w,
                    child: const _PinkBox(),
                  ),
                  Positioned(
                    top: 4.h,
                    right: 20.w,
                    child: const _PinkBox(),
                  ),
                  Positioned(
                    top: 10.h,
                    left: 40.w,
                    child: const _PinkBox(),
                  ),
                  Positioned(
                    top: 4.h,
                    left: 5.w,
                    child: GestureDetector(
                      onTap: onPressedAccessMenu,
                      child: accessMenu,
                    ),
                  ),
                  Positioned(
                    top: 4.h,
                    left: 18.w,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          title,
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w900,
                            fontSize: 18.sp,
                          ),
                        ),
                        Text(
                          descriptionTitle,
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 12.sp,
                          ),
                        )
                      ],
                    ),
                  ),
                  showIconAction
                      ? Positioned(top: 4.h, right: 5.w, child: const Icon(Icons.check_circle, size: 30, color: Colors.white))
                      : const SizedBox(),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 16.5.h),
              child: ClipRRect(
                borderRadius: const BorderRadius.only(topLeft: Radius.circular(40), topRight: Radius.circular(40)),
                child: SingleChildScrollView(
                  physics: scroll ? null : const NeverScrollableScrollPhysics(),
                  child: ConstrainedBox(
                    constraints: BoxConstraints(minHeight: 80.h),
                    child: Container(
                      padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 40),
                      width: MediaQuery.of(context).size.width,
                      decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(40), topRight: Radius.circular(40)),
                      ),
                      child: body,
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              top: 3.h,
              left: 55.w,
              child: ElasticIn(
                duration: const Duration(milliseconds: 2000),
                child: SizedBox(
                  width: 40.w,
                  child: const Image(image: AssetImage('assets/img/petfloat2.png'), fit: BoxFit.contain),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _PinkBox extends StatelessWidget {
  const _PinkBox({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 50,
      height: 50,
      decoration: BoxDecoration(
        color: const Color(0xffC65D7B),
        borderRadius: BorderRadius.circular(80),
      ),
    );
  }
}
