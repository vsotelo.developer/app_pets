import 'package:app_pets/src/theme/ligth.theme.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class CustomTextImputArea extends StatefulWidget {
  const CustomTextImputArea({Key? key, required this.textController, required this.hintText, this.onChanged}) : super(key: key);

  final TextEditingController textController;
  final String hintText;
  final ValueChanged<String>? onChanged;

  @override
  State<CustomTextImputArea> createState() => _CustomTextImputAreaState();
}

class _CustomTextImputAreaState extends State<CustomTextImputArea> {
  // 1F1E22
  Color _backgroudColor = Colors.white;
  Color _borderColor = primaryColor;
  Color _textTitleColor = primaryColor;
  bool _active = false;

  FocusNode focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    focusNode.addListener(() {
      if (focusNode.hasFocus == false) {
        _borderColor = primaryColor;
        _textTitleColor = primaryColor;
        _active = false;
        _backgroudColor = Colors.white;
        setState(() {});
      }
    });
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 6),
      child: Container(
        height: 14.h,
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(30)),
          border: Border.all(color: _borderColor, width: 2.5),
          color: _backgroudColor,
        ),
        child: Padding(
          padding: const EdgeInsets.only(top: 10, left: 15, bottom: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(widget.hintText, style: TextStyle(color: _textTitleColor, fontWeight: FontWeight.bold)),
              Expanded(
                child: SizedBox(
                  child: TextFormField(
                      onChanged: widget.onChanged,
                      controller: widget.textController,
                      focusNode: focusNode,
                      minLines: 6,
                      maxLines: null,
                      keyboardType: TextInputType.multiline,
                      autofocus: false,
                      style: const TextStyle(fontSize: 17, color: Colors.black),
                      cursorColor: primaryColor,
                      decoration: InputDecoration(
                        focusedBorder: InputBorder.none,
                        border: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        suffixIcon: _active
                            ? Transform.translate(
                                offset: const Offset(0, -8),
                                child: null,
                              )
                            : null,
                      ),
                      onTap: () {
                        _borderColor = primaryColor;
                        _textTitleColor = primaryColor;
                        _backgroudColor = primaryColor.withOpacity(0.1);
                        _active = true;
                        setState(() {});
                      }),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
