import 'package:app_pets/src/theme/ligth.theme.dart';
import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  const CustomButton({Key? key, required this.texto, required this.valueOk, this.onPressed}) : super(key: key);
  final String texto;
  final bool valueOk;
  final VoidCallback? onPressed;

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      padding: const EdgeInsets.all(15),
      minWidth: MediaQuery.of(context).size.width,
      color: valueOk ? primaryColor : primaryColor.withOpacity(0.5),
      onPressed: onPressed,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
      child: Text(texto, style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 17, color: Colors.white )),      
    );
  }
}