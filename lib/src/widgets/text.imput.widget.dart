import 'package:app_pets/src/theme/ligth.theme.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class CustomTextImput extends StatefulWidget {
  const CustomTextImput({
    Key? key,
    required this.textController,
    required this.hintText,
    this.textInputType = TextInputType.text,
    this.onChanged,
    this.readOnly = false,
    this.obscureText = false,
  }) : super(key: key);

  final TextEditingController textController;
  final TextInputType textInputType;
  final String hintText;
  final ValueChanged<String>? onChanged;
  final bool readOnly;
  final bool obscureText;

  @override
  State<CustomTextImput> createState() => _CustomTextImputState();
}

class _CustomTextImputState extends State<CustomTextImput> {
  // 1F1E22
  Color _backgroudColor = Colors.white;
  Color _borderColor = primaryColor;
  Color _textTitleColor = primaryColor;
  bool _active = false;

  FocusNode focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    focusNode.addListener(() {
      if (focusNode.hasFocus == false) {
        _borderColor = primaryColor;
        _textTitleColor = primaryColor;
        _active = false;
        _backgroudColor = Colors.white;
        setState(() {});
      }
    });
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 6),
      child: Container(
        height: 8.h,
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(30)),
          border: Border.all(color: _borderColor, width: 2.5),
          color: _backgroudColor,
        ),
        child: Padding(
          padding: EdgeInsets.only(top: 1.h, left: 2.h, bottom: 1.h),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(widget.hintText, style: TextStyle(color: _textTitleColor, fontWeight: FontWeight.bold)),
              Expanded(
                child: SizedBox(
                  child: TextFormField(
                      obscureText: widget.obscureText,
                      readOnly: widget.readOnly,
                      onChanged: widget.onChanged,
                      controller: widget.textController,
                      focusNode: focusNode,
                      keyboardType: widget.textInputType,
                      autofocus: false,
                      style: const TextStyle(fontSize: 17),
                      cursorColor: primaryColor,
                      decoration: InputDecoration(
                        focusedBorder: InputBorder.none,
                        border: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        suffixIcon: !widget.readOnly
                            ? _active
                                ? Transform.translate(
                                    offset: Offset(0, -2.h),
                                    child: IconButton(
                                        splashColor: Colors.transparent,
                                        icon: Icon(Icons.cancel_rounded, color: primaryColor, size: 5.w),
                                        onPressed: () {
                                          widget.textController.text = '';
                                          _backgroudColor = Colors.white;
                                          setState(() {});
                                        }),
                                  )
                                : null
                            : null,
                      ),
                      onTap: () {
                        if (!widget.readOnly) {
                          _borderColor = primaryColor;
                          _textTitleColor = primaryColor;
                          _backgroudColor = primaryColor.withOpacity(0.1);
                          _active = true;
                          setState(() {});
                        }
                      }),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
