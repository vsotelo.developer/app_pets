import 'package:flutter/material.dart';

ThemeData ligthTheme(BuildContext context) {
  return ThemeData.light().copyWith(
    colorScheme: ColorScheme.fromSwatch(
      accentColor: primaryColor, // but now it should be declared like this
    ),
    textTheme: Theme.of(context).textTheme.apply(
          fontFamily: 'DMSans',
        ),
  );
}
Color primaryColor = const Color(0xffB6052C);
