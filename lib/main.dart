import 'package:app_pets/src/bloc/drawer/drawer_bloc.dart';
import 'package:app_pets/src/bloc/login/login_bloc.dart';
import 'package:app_pets/src/bloc/core/core_bloc.dart';
import 'package:app_pets/src/screens/check.auth.login.dart';
import 'package:app_pets/src/service/fcm.service.dart';
import 'package:app_pets/src/theme/ligth.theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_zoom_drawer/config.dart';
import 'package:sizer/sizer.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await PushNotificationServices.initializeApp();
  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider(create: (_) => DrawerBloc()),
        BlocProvider(create: (_) => LoginBloc()),        
        BlocProvider(create: (_) => CoreBloc()),        
      ],
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Sizer(builder: (BuildContext context, _, __) {
      _removeColorTop();
      return MaterialApp(
        title: 'AlmaVet App',
        theme: ligthTheme(context),
        debugShowCheckedModeBanner: false,
        home: const CheckAuthScreen(),
      );
    });
  }
}

final ZoomDrawerController zommDrawerController = ZoomDrawerController();

void _removeColorTop() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
    statusBarColor: primaryColor,
  ));
}
